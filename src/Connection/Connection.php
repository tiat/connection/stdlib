<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Stdlib\Connection;

//
use Tiat\Connection\Stdlib\Exception\InvalidArgumentException;

use function ctype_alnum;
use function sprintf;
use function str_replace;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait Connection {
	
	/**
	 * @var null|array
	 * @since   3.0.0 First time introduced.
	 */
	protected ?array $_connectionError;
	
	/**
	 * @var null|array
	 * @since   3.0.0 First time introduced.
	 */
	protected ?array $_connectionInfo;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	protected string $_connectionName;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionInfo() : ?array {
		return $this->_connectionInfo ?? NULL;
	}
	
	/**
	 * @param    array    $info
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionInfo(array $info) : ConnectionInterface {
		//
		if(! empty($info)):
			$this->_connectionInfo = $info;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionError() : ?array {
		//
		return $this->_connectionError ?? NULL;
	}
	
	/**
	 * @param    string    $error
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionError(string $error) : ConnectionInterface {
		//
		if(! empty($error)):
			$this->_connectionError[] = $error;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : ConnectionInterface {
		//
		$test = str_replace(['_', '-'], "", $name);
		
		//
		if(! empty($test) && ctype_alnum($test)):
			$this->_connectionName = $name;
		else:
			$msg =
				sprintf("Name can contain only alphanumeric character(s). Also dash(-) and/or underscore(_) are allowed with character(s). Got '%s'",
				        $name);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_connectionName ?? NULL;
	}
}
