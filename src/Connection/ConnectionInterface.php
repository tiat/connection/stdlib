<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Stdlib\Connection;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ConnectionInterface {
	
	/**
	 * @param    mixed    $result
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionResult(mixed $result) : ConnectionInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionResult() : mixed;
	
	/**
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetConnectionResult() : ConnectionInterface;
	
	/**
	 * @param    array    $info
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionInfo(array $info) : ConnectionInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionInfo() : mixed;
	
	/**
	 * @param    string    $error
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setConnectionError(string $error) : ConnectionInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getConnectionError() : mixed;
	
	/**
	 * Set connection name
	 *
	 * @param    string    $name
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : ConnectionInterface;
	
	/**
	 * Get connection name
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string;
	
	/**
	 * Initialize connection
	 *
	 * @param ...$args
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : ConnectionInterface;
	
	/**
	 * Run the Curl request
	 *
	 * @param ...$args
	 *
	 * @return ConnectionInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : ConnectionInterface;
}
