<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Connection/Stdlib
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Connection\Stdlib\Adapter;

//
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum ConnectionParamKeywords: string implements InterfaceEnumString {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use TraitEnum;
	
	/**
	 * Represents the cache connection parameter.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case CACHE = 'cache';
	
	/**
	 * Represents the charset used for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case CHARSET = 'charset';
	
	/**
	 * Represents the database name for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case DATABASE = 'database';
	
	/**
	 * Represents the Data Source Name (DSN) for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case DSN = 'dsn';
	
	/**
	 * Represents the driver used for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case DRIVER = 'driver';
	
	/**
	 * Represents the hostname of the server for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case HOSTNAME = 'hostname';
	
	/**
	 * Represents the password for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case PASSWORD = 'password';
	
	/**
	 * Represents the port number for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case PORT = 'port';
	
	/**
	 * Represents the socket used for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case SOCKET = 'socket';
	
	/**
	 * Represents the timeout value for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case TIMEOUT = 'timeout';
	
	/**
	 * Represents the username for the connection.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case USERNAME = 'username';
	
	/**
	 * Represents the weight parameter for load balancing.
	 *
	 * @since 3.0.0 First time introduced.
	 */
	case WEIGHT = 'weight';
}
